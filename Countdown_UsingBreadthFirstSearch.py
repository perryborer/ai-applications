"""
Basic application to demonstrate an uninformed breadth first search approach to solve Countdown style number puzzles.

Inspired by pseudo code in Artificial Intelligence A Modern Approach textbook.

Application will search for a way to combine the option numbers alongside + - * / operators to reach the target number
For example, if option numbers are 3, 4, 5 and 7 and the target number is 23, app may find solution 4 * 5 + 3
Brackets are not used in this implementation.

- Note: This is a more basic application than the others in this folder. 
        A search algorithm isn't necessarily the most appropriate approach for this type of problem, nor is this implementation optimised.
        Entering many option numbers can cause the process to take extremely long.

Ask user for goal number
Ask user for option numbers
Initialise initial state and frontier
Begin loop
    If frontier is empty return failure
    Choose a leaf node and remove from frontier
    If the node contains a goal state then return the solution
    expand the the chosen node, adding the resulting nodes to the frontier
"""


import sys


#Defines a mathematical action in the form of an operator and a number, for example, +3 or *7
class Action:
    def __init__(self, operator, number):
        self.operator = operator
        self.number = number


#Defines a node object which posseses a link to a parent node plus a new action                    
class Node:
    def __init__(self, parent_id, action):
        self.id = generate_node_id()
        self.parent_id = parent_id
        self.action = action
        if (parent_id):
            self.path = nodes[parent_id].path + self.action.operator + self.action.number
        else:
            self.path = ""
        if (parent_id):
            self.numbers_used = nodes[parent_id].numbers_used + [self.action.number]
        else:
            self.numbers_used = []

    def is_goal_achieved(self):
        global goal_number
        #print("Evaluating: " + self.path)
        if (self.path == ""):
            if (goal_number == 0):
                return True
            else:
                return False
        else:
            #print("Result: " + str(eval(self.path)))
            return eval(self.path) == goal_number

    def remove_from_frontier(self):
        global frontier
        del frontier[self.id]

    def expand_frontier(self):
        global nodes
        global frontier
        available_actions = self.generate_available_actions()
        for action in available_actions:
            new_node = Node(self.id, action)
            nodes[new_node.id] = new_node
            frontier[new_node.id] = new_node

    def generate_available_actions(self):
        available_actions = []
        global option_numbers
        numbers_remaining = option_numbers[:]
        for number in self.numbers_used:
            numbers_remaining.remove(number)
        if (self.parent_id):
            for number in numbers_remaining:
                available_actions.append(Action("+", number))
                available_actions.append(Action("-", number))
                available_actions.append(Action("*", number))
                available_actions.append(Action("/", number))
            return available_actions
        else:
            for number in numbers_remaining:
               available_actions.append(Action("", number)) 
            return available_actions


def generate_node_id():
    global newest_node_id
    newest_node_id += 1
    return newest_node_id


def generate_initial_node():
    new_node = Node(None, None)
    global nodes
    global frontier
    nodes[new_node.id] = new_node
    frontier[new_node.id] = new_node


def check_for_empty_frontier():
    global frontier
    global search_continues
    if (len(frontier) == 0):
        print("The frontier is empty. The search has not found a solution.")
        input("Press enter to exit")
        search_continues = False


def get_node_from_frontier():
    global frontier
    return frontier[list(frontier.keys())[0]] 
    # As nodes are expanded from the frontier in order of creation - this results in a breadth-first search
    

# Initial setup
nodes = {}
frontier = {}
search_continues = True
newest_node_id = 0
generate_initial_node()


# Begin application
goal_number_input = input("Enter a goal number: ")
goal_number = int(goal_number_input)
print("Goal number is: ", goal_number)
option_numbers_input = input("Enter one or more option numbers (four or five numbers is reccomended). Separate each option number with a space: ")
option_numbers = option_numbers_input.split()
print("Please wait...")


# Begin search
while search_continues == True:    
    selected_node = get_node_from_frontier()
    selected_node.remove_from_frontier()

    if (selected_node.is_goal_achieved() == True):
        print("Target number achieved! Using solution: " + selected_node.path)
        search_continues = False
        input("Press enter to exit")

    else:
        selected_node.expand_frontier()

    check_for_empty_frontier()