# Application to demonstrate minimax algorithm applied to the game of Noughts and Crosses (aka Tic Tac Toe)

# Instructions:
# Follow on-screen prompts.
# Allow the computer a little while to consider its move, especially when it goes first as it has a lot of outcomes to consider.
# Observe the console window to see the number of game states considered for each computer turn.


import pygame
import os
import random


# Constants
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
PINK = (255, 153, 153)

# Classes
class Board:
    def __init__(self):
        self.squares = []
        self.next_to_move = 'x'

    def set_default_squares(self):
        self.squares = [Square(0, 0, 'empty'), Square(0, 1, 'empty'), Square(0, 2, 'empty'),
                        Square(1, 0, 'empty'), Square(1, 1, 'empty'), Square(1, 2, 'empty'),
                        Square(2, 0, 'empty'), Square(2, 1, 'empty'), Square(2, 2, 'empty')]

    def copy_and_increment_board_state(self, move):
    # Returns a copy of this board, including copies of square objects, with one square updated as described by the move argument provided
        global game_states_considered
        game_states_considered += 1
        new_board = Board()
        for square in self.squares:
            if square.row == move.row and square.column == move.column:
                new_board.squares += [Square(move.row, move.column, move.content)]
            else:
                new_board.squares += [Square(square.row, square.column, square.content)]
        if self.next_to_move == 'x':
            new_board.next_to_move = 'o'
        elif self.next_to_move == 'y':
            new_board.next_to_move = 'o'
        return new_board

    def check_for_winner(self):
        square_states = []
        for square in self.squares:
            square_states += [[square.row, square.column, square.content]]
        if (([0, 0, 'x'] in square_states and [0, 1, 'x'] in square_states and [0, 2, 'x'] in square_states)
            or ([1, 0, 'x'] in square_states and [1, 1, 'x'] in square_states and [1, 2, 'x'] in square_states)
            or ([2, 0, 'x'] in square_states and [2, 1, 'x'] in square_states and [2, 2, 'x'] in square_states)
            or ([0, 0, 'x'] in square_states and [1, 0, 'x'] in square_states and [2, 0, 'x'] in square_states)
            or ([0, 1, 'x'] in square_states and [1, 1, 'x'] in square_states and [2, 1, 'x'] in square_states)
            or ([0, 2, 'x'] in square_states and [1, 2, 'x'] in square_states and [2, 2, 'x'] in square_states)
            or ([0, 0, 'x'] in square_states and [1, 1, 'x'] in square_states and [2, 2, 'x'] in square_states)
            or ([0, 2, 'x'] in square_states and [1, 1, 'x'] in square_states and [2, 0, 'x'] in square_states)):
            return 'x has won'
        elif (([0, 0, 'o'] in square_states and [0, 1, 'o'] in square_states and [0, 2, 'o'] in square_states)
            or ([1, 0, 'o'] in square_states and [1, 1, 'o'] in square_states and [1, 2, 'o'] in square_states)
            or ([2, 0, 'o'] in square_states and [2, 1, 'o'] in square_states and [2, 2, 'o'] in square_states)
            or ([0, 0, 'o'] in square_states and [1, 0, 'o'] in square_states and [2, 0, 'o'] in square_states)
            or ([0, 1, 'o'] in square_states and [1, 1, 'o'] in square_states and [2, 1, 'o'] in square_states)
            or ([0, 2, 'o'] in square_states and [1, 2, 'o'] in square_states and [2, 2, 'o'] in square_states)
            or ([0, 0, 'o'] in square_states and [1, 1, 'o'] in square_states and [2, 2, 'o'] in square_states)
            or ([0, 2, 'o'] in square_states and [1, 1, 'o'] in square_states and [2, 0, 'o'] in square_states)):
            return 'o has won'
        else:
            return 'noone has won'

    def get_available_moves(self):
    # returns a list of all available moves, the best outcome for x is unknwon
        available_moves = []
        for square in self.squares:
            if square.content == 'empty':
                available_moves += [Move(square.row, square.column, self.next_to_move)]
        return available_moves

    def calculate_moves(self):
    # returns a list of all available moves, with the best outcome for x calulated for each move
        available_moves = self.get_available_moves()
        calculated_moves = []
        global game_states_considered
        game_states_considered = 0
        for move in available_moves:
            if computer_token == 'x':
                move.best_outcome_for_x = self.calculate_best_outcome_for_x(move)
            if computer_token == 'o':
                move.best_outcome_for_x = self.calculate_best_outcome_for_o(move)
            calculated_moves += [move]
        print("Game states considered: ", game_states_considered)
        return calculated_moves

    def calculate_best_outcome_for_x(self, move):
        successor_board = self.copy_and_increment_board_state(move)
        if successor_board.check_for_winner() == 'x has won':
            return 1
        else:
            subsequent_moves = successor_board.get_available_moves()
            if subsequent_moves == []:
                # this means that noone has won but the board is full - i.e. it is a draw, so set to 0
                return 0
            else:
                outcomes = []             
                for subsequent_move in subsequent_moves:
                    outcomes += [successor_board.calculate_best_outcome_for_o(subsequent_move)]
                return min(outcomes)


    def calculate_best_outcome_for_o(self, move):
        successor_board = self.copy_and_increment_board_state(move)
        if successor_board.check_for_winner() == 'o has won':
            return -1
        else:
            subsequent_moves = successor_board.get_available_moves()
            if subsequent_moves == []:
                # this means that noone has won but the board is full - i.e. it is a draw, so set to 0
                return 0
            else:  
                outcomes = []
                for subsequent_move in subsequent_moves:
                    outcomes += [successor_board.calculate_best_outcome_for_x(subsequent_move)]
                return max(outcomes)

    def get_best_moves(self, moves):
        best_outcome_so_far = 0
        best_moves = []
        for move in moves:
            if move.best_outcome_for_x == best_outcome_so_far:
                best_moves += [move]
            if computer_token == 'x':
                if move.best_outcome_for_x > best_outcome_so_far:
                    best_moves = [move]
                    best_outcome_so_far = move.best_outcome_for_x
            if computer_token == 'o':                
                if move.best_outcome_for_x < best_outcome_so_far:
                    best_moves = [move] 
                    best_outcome_so_far = move.best_outcome_for_x
        return best_moves
    
    def draw(self):
        self.lines = [((325, 75), (325, 525)),
                        ((475, 75), (475, 525)),
                        ((175, 225), (625, 225)),
                        ((175, 375), (625, 375))]
        for line in self.lines:
            pygame.draw.line(window, BLACK, line[0], line[1], 3)
        for square in self.squares:
            square.draw()

    def locate_square(self, coordinates):
        for square in self.squares:
            if (coordinates[0] >= square.x_lower_bound 
                and coordinates[0] < square.x_upper_bound 
                and coordinates[1] >= square.y_lower_bound
                and coordinates[1] < square.y_upper_bound):
                return square
        return None


class Square:
    def __init__(self, row, column, content):
        self.row = row
        self.column = column
        self.content = content
        self.x_lower_bound = 175 + (self.column * 150)
        self.x_upper_bound = 325 + (self.column * 150)
        self.y_lower_bound = 75 + (self.row * 150)
        self.y_upper_bound = 225 + (self.row * 150)

    def draw(self):              
        if self.content == 'x':
            pygame.draw.line(window,
                                BLUE,
                                (self.x_lower_bound + 25, self.y_lower_bound + 25),
                                (self.x_upper_bound - 25, self.y_upper_bound - 25),
                                3)
            pygame.draw.line(window,
                                BLUE,
                                (self.x_upper_bound - 25, self.y_lower_bound + 25),
                                (self.x_lower_bound + 25, self.y_upper_bound - 25),
                                3)
        elif self.content == 'o':
            pygame.draw.circle(window, GREEN, (self.x_lower_bound + 75, self.y_upper_bound - 75), 50, 3)


class Move:
    def __init__(self, row, column, content):
        self.row = row
        self.column = column
        self.content = content
        self.best_outcome_for_x = 'unknown' # 1 = win for x, 0 = draw, -1 = loss for x, 'unknown' = not yet determined

    def create_text_box(self):
        x_pos = 205 + (self.column * 150)
        y_pos = 135 + (self.row * 150)
        self.text_box = Text_Box(x_pos, y_pos, 50, 30, PINK, 22, get_outome_word(self.best_outcome_for_x, computer_token))


class Text_Box:
    def __init__(self, x_pos, y_pos, height, width, colour, font_size, text):
        self.colour = colour
        self.rect = pygame.Rect(x_pos, y_pos, height, width)
        self.font = pygame.font.Font('freesansbold.ttf', font_size)
        self.text_surface = self.font.render(text, True, BLACK)
    
    def draw(self):
        pygame.draw.rect(window, self.colour, self.rect)
        window.blit(self.text_surface, self.rect)


def get_outome_word(best_outcome_for_x, token):
    if ((best_outcome_for_x == 1 and token == 'x')
        or (best_outcome_for_x == -1 and token == 'o')):
        return "Win"
    elif ((best_outcome_for_x == 0 and token == 'x')
        or (best_outcome_for_x == 0 and token == 'o')):
        return "Draw"
    elif ((best_outcome_for_x == -1 and token == 'x')
        or (best_outcome_for_x == 1 and token == 'o')):
        return "Loss"


# For testing specific states
sample_squares = [Square(0, 0, 'x'), Square(0, 1, 'o'), Square(0, 2, 'x'),
                    Square(1, 0, 'empty'), Square(1, 1, 'x'), Square(1, 2, 'empty'),
                    Square(2, 0, 'empty'), Square(2, 1, 'o'), Square(2, 2, 'o')]


# Initial setup
window_pos_x = 340
window_pos_y = 50
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (window_pos_x, window_pos_y)
pygame.init()
window = pygame.display.set_mode((800, 725))
pygame.display.set_caption('Noughts and Crosses using Minimax algorithm')
clock = pygame.time.Clock()
board = Board()
board.set_default_squares()
#board.squares = sample_squares
game_states_considered = 0
app_state = 'choose_x_or_o'
player_token = 'undecided'
computer_token = 'undecided'
appExit = False

play_as_x_button = Text_Box(175, 575, 200, 25, PINK, 20, 'Go 1st - play as X')
play_as_y_button = Text_Box(425, 575, 200, 25, PINK, 20, 'Go 2nd - play as O')
your_turn_text_box = Text_Box(150, 575, 510, 25, PINK, 20, 'It is your turn to move, click on your desired square')
computer_will_consider_text_box1 = Text_Box(90, 575, 630, 25, PINK, 16,
                                            'The computer will consider the available moves, and calculate the best outcome ')
computer_will_consider_text_box2 = Text_Box(90, 600, 630, 25, PINK, 16,
                                            'of each (assuming optimal counter-play). Click anywhere to continue...')
computer_calculating_text_box = Text_Box(150, 575, 510, 25, PINK, 20, 'Please wait a moment, the computer is calculating...')
player_has_won_text_box = Text_Box(175, 575, 430, 25, PINK, 20, 'You have won! Press enter to reset')                                            
computer_has_won_text_box = Text_Box(175, 575, 430, 25, PINK, 20, 'The computer has won! Press enter to reset')
game_is_draw_text_box = Text_Box(175, 575, 430, 25, PINK, 20, 'The games is a draw! Press enter to reset')


# App loop
while not appExit:
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            gameExit = True
            pygame.quit()
            quit()

        elif app_state == 'choose_x_or_o':
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = event.pos
                if play_as_x_button.rect.collidepoint(mouse_pos):
                    print("x selected")
                    player_token = 'x'
                    computer_token = 'o'
                    app_state = 'player_turn'
                elif play_as_y_button.rect.collidepoint(mouse_pos):
                    print("o selected")
                    player_token = 'o'
                    computer_token = 'x'
                    app_state = 'computer_will_consider'

        elif app_state == 'player_turn':
            if event.type == pygame.MOUSEBUTTONDOWN:
                square = board.locate_square(pygame.mouse.get_pos())
                if square and square.content == 'empty':
                    square.content = player_token
                    if board.check_for_winner() != 'noone has won':
                        app_state = 'player_has_won'
                    else:
                        if board.get_available_moves() == []:
                            app_state = 'game_is_draw'
                        else:
                            app_state = 'computer_will_consider'
                        board.next_to_move = computer_token

        elif app_state == 'computer_will_consider':
            if event.type == pygame.MOUSEBUTTONDOWN:
                app_state = 'computer_calculating'

        elif app_state == 'computer_calculating':
            calculated_moves = board.calculate_moves()
            for move in calculated_moves:
                print('row: ', move.row, 'column: ', move.column, 'content: ', move.content, 'best outcome for x: ', move.best_outcome_for_x)
                move.create_text_box()
            best_moves = board.get_best_moves(calculated_moves)

            best_outcome_for_computer = best_moves[0].best_outcome_for_x # all moves in best moves share the same outcome
            summary_text = ('The best outcome the computer can ensure is a ' + 
                            get_outome_word(best_outcome_for_computer, computer_token) +
                            '. This can be achieved in ' +
                            str(len(best_moves)) +
                            ' way(s).')
            summary_text_box1 = Text_Box(70, 575, 670, 25, PINK, 16, summary_text)
            summary_text_box2 = Text_Box(70, 600, 670, 25, PINK, 16,
                                        'The computer will randomly select one move to achieve this goal. Click to continue...')    

            app_state = 'best_outcome_summary'

        elif app_state == 'best_outcome_summary':
            if event.type == pygame.MOUSEBUTTONDOWN:
                chosen_move = random.choice(best_moves)
                board = board.copy_and_increment_board_state(chosen_move)
                if board.check_for_winner() != 'noone has won':
                    app_state = 'computer_has_won'
                else:
                    if board.get_available_moves() == []:
                        app_state = 'game_is_draw'
                    else:
                        app_state = 'player_turn'

        elif app_state == 'player_has_won' or app_state == 'computer_has_won' or app_state == 'game_is_draw':
            if event.type==pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    board = Board()
                    board.set_default_squares()
                    app_state = 'choose_x_or_o'

    # Create display
    window.fill(WHITE)
    board.draw()

    if app_state == 'choose_x_or_o':
        play_as_x_button.draw()
        play_as_y_button.draw()

    elif app_state == 'player_turn':
        your_turn_text_box.draw()

    elif app_state == 'computer_will_consider':
        computer_will_consider_text_box1.draw()
        computer_will_consider_text_box2.draw()

    elif app_state == 'computer_calculating':
        computer_calculating_text_box.draw()

    elif app_state == 'best_outcome_summary':
        for move in calculated_moves:
            move.text_box.draw()
        summary_text_box1.draw()
        summary_text_box2.draw()

    elif app_state == 'player_has_won':
        player_has_won_text_box.draw()

    elif app_state == 'computer_has_won':
        computer_has_won_text_box.draw()

    elif app_state == 'game_is_draw':
        game_is_draw_text_box.draw()

    pygame.display.update()
    clock.tick(60)