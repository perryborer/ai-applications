Here are a few basic Python applications that use AI search techniques to solve different problems.

These were written by me prior to me starting my MSc in AI.

These applications are demoed in a short video here:
https://www.loom.com/share/631f5a0f8dae4fd9a4dc4816b448952c

Read the comments at the top of each file for a description of the application.

The applications are all written in Python 3.

Both RouteFinder_UsingAStarSearch and NoughtsAndCrosses_UsingMinimax applications require pygame to be installed. I.e type: 
pip install pygame
from within a python console.

Countdown_UsingBreadthFirstSearch application is more basic than the other apps and perhaps of less interest.

