# Application to demonstrate using A* Search to route-find in 2-dimensional space

# Instructions: 
# - Press enter to calculate the shortest route between the red dots
# - Click and drag to draw lines and update the route
# - Press escape to clear lines, i.e. to restart

# Note:
# - When no route is possible, this search implementation will keep searching in vain

import pygame
import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Line:
    def __init__(self, end_point_1, end_point_2):
        self.end_point_1 = end_point_1
        self.end_point_2 = end_point_2          


class Node:
    def __init__(self, x, y, parent_node):
        self.x = x
        self.y = y
        self.parent_node = parent_node
        if parent_node:
            self.cost_from_parent = calculate_distance(self.parent_node.x, self.parent_node.y, self.x, self.y)
            self.cost_from_origin = self.parent_node.cost_from_origin + self.cost_from_parent       
        else: 
            self.cost_from_parent = 0
            self.cost_from_origin = 0  
        self.estimated_cost_to_goal = calculate_distance(self.x, self.y, goal_point.x, goal_point.y)
        self.estimated_total_cost = self.cost_from_origin + self.estimated_cost_to_goal

    def goal_reached(self):
        if self.x == goal_point.x and self.y == goal_point.y:
            return True
        else:
            return False

    def expand(self):
        accessible_points = self.calculate_accessible_points()
        for point in accessible_points:
            frontier_nodes.append(Node(point.x, point.y, self))
        frontier_nodes.remove(self)

    def calculate_accessible_points(self):
        # Calculate accessible points by removing from a list of all points, the point of the current node itself,
        # and points that cannot be travelled to without crossing over a line.
        accessible_points = points.copy()
        for point in points:
            if self.x == point.x and self.y == point.y:
                    accessible_points.remove(point) 
            for line in lines:
                if do_intersect((self.x, self.y), (point.x, point.y), (line.end_point_1.x, line.end_point_1.y), (line.end_point_2.x, line.end_point_2.y)):
                    accessible_points.remove(point)
                    # Break from for line in lines loop because point does not need to be removed from accessible_points again,
                    # even if obstructed by a different line
                    break 
        return accessible_points


def calculate_distance(x1, y1, x2, y2):
    distance = math.sqrt(((x1 - x2)**2 + (y1 - y2)**2))
    return distance


def unzip_node(node):
# Constructs an ordered list of coordinates (i.e. the route) by unzipping a node's parent recursively
    node = node
    route = []
    route.insert(0, (node.x, node.y))
    while node.parent_node:
        node = node.parent_node
        route.insert(0, (node.x, node.y))
    return route


# The three methods below I've taken from the internet that check if two lines intersect, modified to return false if two lines meet at an endpoint.
def on_segment(p, q, r):
    '''Given three colinear points p, q, r, the function checks if 
    point q lies on line segment "pr"
    '''
    if (q[0] <= max(p[0], r[0]) and q[0] >= min(p[0], r[0]) and
        q[1] <= max(p[1], r[1]) and q[1] >= min(p[1], r[1])):
        return True
    return False


def orientation(p, q, r):
    '''Find orientation of ordered triplet (p, q, r).
    The function returns following values
    0 --> p, q and r are colinear
    1 --> Clockwise
    2 --> Counterclockwise
    '''

    val = ((q[1] - p[1]) * (r[0] - q[0]) - 
            (q[0] - p[0]) * (r[1] - q[1]))
    if val == 0:
        return 0  # colinear
    elif val > 0:
        return 1   # clockwise
    else:
        return 2  # counter-clockwise


def do_intersect(p1, q1, p2, q2):
    '''Main function to check whether the closed line segments p1 - q1 and p2 
       - q2 intersect'''
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    # My modification to return false when two lines meet on a point, i.e. do not 'cross' in common parlance
    if p1 == p2 or p1 == q2 or q1 == p2 or q1 == q2:
        return False

    # General case
    if (o1 != o2 and o3 != o4):
        return True

    # Special Cases
    # p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 and on_segment(p1, p2, q1)):
        return True

    # p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 and on_segment(p1, q2, q1)):
        return True

    # p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 and on_segment(p2, p1, q2)):
        return True

    # p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 and on_segment(p2, q1, q2)):
        return True

    return False # Doesn't fall in any of the above cases


def find_route():
    while frontier_nodes != []:
        node = min(frontier_nodes, key = lambda n : n.estimated_total_cost)
        if node.goal_reached():
            return unzip_node(node)
        else:  
            node.expand()
    return None # Return None as the frontier is empty with no solutions found


def reset_search():
    global frontier_nodes
    frontier_nodes = [Node(origin_point.x, origin_point.y, None)]
    global route_found
    route_found = None


def reset_lines():
    global lines
    lines = []
    global points
    points = [goal_point]


# Initial setup
pygame.init()
window = pygame.display.set_mode((800, 600))
pygame.display.set_caption('Route-finder using A* Search')
clock = pygame.time.Clock()

goal_point = Point(700, 100)
origin_point = Point(100, 500)
points = [origin_point, goal_point]
lines = []
frontier_nodes = [Node(origin_point.x, origin_point.y, None)]
route_found = None

appExit = False
mouse_pressed = False


# Application loop
while not appExit:
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            appExit = True
            pygame.quit()
            quit()

        if event.type == pygame.MOUSEBUTTONDOWN:
            line_end_point_1 = Point(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
            mouse_pressed = True

        if event.type == pygame.MOUSEBUTTONUP:
            line_end_point_2 = Point(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
            line = Line(line_end_point_1, line_end_point_2)
            points.append(line_end_point_1)
            points.append(line_end_point_2)
            lines.append(line)
            mouse_pressed = False
            reset_search()
            route_found = find_route()
            print(route_found)

        if event.type==pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                reset_search()
                route_found = find_route()
                print(route_found)

        if event.type==pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                reset_search()
                reset_lines()

    # Draw content
    window.fill((255, 255, 255))
    for line in lines:
        pygame.draw.line(window, (0, 255, 0), (line.end_point_1.x, line.end_point_1.y), (line.end_point_2.x, line.end_point_2.y))
    pygame.draw.circle(window, (255, 0, 0), (origin_point.x, origin_point.y), 10, 0)
    pygame.draw.circle(window, (255, 0, 0), (goal_point.x, goal_point.y), 10, 0)
    if route_found:
        previous_coordinate = None
        for coordinate in route_found:
            if previous_coordinate:
                pygame.draw.line(window, (255, 0, 0), (previous_coordinate[0], previous_coordinate[1]), (coordinate[0], coordinate[1]))
            previous_coordinate = coordinate
    if mouse_pressed:
        pygame.draw.line(window, (0, 0, 255), (line_end_point_1.x, line_end_point_1.y), (pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]))

    pygame.display.update()
    clock.tick(60)